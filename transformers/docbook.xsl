<?xml version="1.0"?>
<xsl:stylesheet xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:d="http://docbook.org/ns/docbook" xmlns:axf="http://www.antennahouse.com/names/XSL/Extensions" xmlns:xlink="https://www.w3.org/1999/xlink" version="1.0" exclude-result-prefixes="d">
  <xsl:import href="/usr/local/Cellar/docbook-xsl/1.79.1//docbook-xsl-ns/fo/docbook.xsl"/>
  <xsl:param name="paper.type" select="'A4'"/>
  <!-- Fügen Sie hier weitere Optionen ein -->
  <xsl:param name="body.margin.bottom">20pt</xsl:param>
  <xsl:param name="body.start.indent">0pc</xsl:param>
  <xsl:param name="body.font.family">Alegreya, serif</xsl:param>
  <xsl:param name="title.font.family">AlegreyaSans, sans</xsl:param>
  <xsl:param name="sans.font.family">AlegreyaSans, sans </xsl:param>
  <xsl:param name="monospace.font.family">"TelderMonoHT" </xsl:param>
</xsl:stylesheet>
