import glob
import subprocess
from pathlib import Path

import xml.sax as sax

variants = "Bold BoldItalic Italic Regular".split()
fonts = "Alegreya AlegreyaSans AlegreyaSC AlegreyaSansSC".split()

def make_xml(f, font, variant):
            pth = Path('./fonts') / f"{font}-{variant}.ttf"
            print(pth)
            target_path = Path('./fonts') / (pth.stem + ".xml")
            if True or not target_path.exists():
                subprocess.check_call(["java", "-jar", "packaged_ttf_reader-0.1-SNAPSHOT-jar-with-dependencies.jar", str(pth), str(target_path)])

                metric = target_path.resolve()
                embed = pth.resolve()

                if "Bold" in variant:
                    weight = "bold"
                else:
                    weight = "normal"

                if "Italic" in variant:
                    style = "italic"
                else:
                    style = "normal"

                f.write(f'<font metrics-url="file://{metric}" kerning="yes" embed-url="file://{embed}">'
                    f'<font-triplet name="{font}" style="{style}" weight="{weight}" /></font>\n')

with open("generated-cfg.xml", "w") as f:
    for font in fonts:
        for variant in variants:
            make_xml(f, font, variant)

    make_xml(f, "TelderMonoHT", "Regular")
    make_xml(f, "TelderMonoHT", "Bold")