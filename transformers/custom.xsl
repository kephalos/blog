<?xml version="1.0"?>
<xsl:stylesheet xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:d="http://docbook.org/ns/docbook" version="1.0" xpath-default-namespace="http://docbook.org/ns/docbook" exclude-result-prefixes="d">
  <xsl:output indent="yes"/>
  <xsl:param name="body.font.family">Alegreya, serif</xsl:param>
  <xsl:param name="body.font.size">12pt</xsl:param>
  <xsl:param name="body.line.height">1.1em</xsl:param>
  <xsl:param name="body.margin.bottom">5pt</xsl:param>
  <xsl:param name="body.margin.top">0pt</xsl:param>
  <xsl:param name="body.start.indent">1pc</xsl:param>
  <xsl:param name="body.start.vspace">0pc</xsl:param>
  <xsl:param name="monospace.font.family">"TelderMonoHT" </xsl:param>
  <xsl:param name="page.width">210mm</xsl:param>
  <xsl:param name="page.height">297mm</xsl:param>
  <xsl:param name="page.margin.bottom">20mm</xsl:param>
  <xsl:param name="page.margin.inner">10mm</xsl:param>
  <xsl:param name="page.margin.outer">20mm</xsl:param>
  <xsl:param name="page.margin.top">20mm</xsl:param>
  <xsl:param name="region.after.extent">0pt</xsl:param>
  <xsl:param name="region.before.extent">0pt</xsl:param>
  <xsl:template match="node()|@*">
    <xsl:apply-templates select="node()|@*"/>
  </xsl:template>
  <xsl:template name="generate-table-of-contents">
    <fo:block break-before="page">
      <fo:block font-size="{$body.font.size}" font-family="{$body.font.family}" font-weight="bold">Inhalt</fo:block>
      <xsl:for-each select="//d:chapter">
        <fo:block font-size="{$body.font.size}" font-family="{$body.font.family}" text-align-last="justify">
          <fo:basic-link internal-destination="{generate-id(.)}"><xsl:value-of select="count(preceding::d:chapter) + 1"/><xsl:text> </xsl:text>
		    Kapitel
            <fo:leader leader-pattern="dots"/>
            <fo:page-number-citation ref-id="{generate-id(.)}"/>
          </fo:basic-link>
        </fo:block>
      </xsl:for-each>
    </fo:block>
  </xsl:template>
  <xsl:template match="d:book">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="titlepage" font-family="Alegreya" page-height="{$page.height}" page-width="{$page.width}" margin-left="10mm" margin-right="10mm" margin-top="{$page.margin.top}" margin-bottom="{$page.margin.bottom}">
          <fo:region-body margin-top="{$body.margin.top}" margin-bottom="{$body.margin.bottom}"/>
          <fo:region-before extent="{$region.before.extent}"/>
          <fo:region-after extent="{$region.after.extent}"/>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="PageMaster.Inhalt-rechts" font-family="Alegreya" page-height="{$page.height}" page-width="{$page.width}" margin-left="{$page.margin.inner}" margin-right="{$page.margin.outer}" margin-top="{$page.margin.top}" margin-bottom="{$page.margin.bottom}">
          <fo:region-body column-count="2" margin-top="{$body.margin.top}" margin-bottom="{$body.margin.bottom}"/>
          <fo:region-before region-name="Inhalt-rechts-davor" extent="{$region.before.extent}"/>
          <fo:region-after region-name="Inhalt-rechts-danach" extent="{$region.after.extent}"/>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="PageMaster.Inhalt-links" font-family="Alegreya" page-height="{$page.height}" page-width="{$page.width}" margin-left="{$page.margin.outer}" margin-right="{$page.margin.inner}" margin-top="{$page.margin.top}" margin-bottom="{$page.margin.bottom}">
          <fo:region-body margin-top="{$body.margin.top}" margin-bottom="{$body.margin.bottom}"/>
          <fo:region-before region-name="Inhalt-links-davor" extent="{$region.before.extent}"/>
          <fo:region-after region-name="Inhalt-links-danach" extent="{$region.after.extent}"/>
        </fo:simple-page-master>
        <fo:simple-page-master font-family="Alegreya" master-name="main" page-height="{$page.height}" page-width="{$page.width}" margin-left="10mm" margin-right="10mm" margin-top="{$page.margin.top}" margin-bottom="{$page.margin.bottom}">
          <fo:region-body margin-top="{$body.margin.top}" margin-bottom="{$body.margin.bottom}"/>
          <fo:region-before extent="{$region.before.extent}"/>
          <fo:region-after extent="{$region.after.extent}"/>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="Inhalt-Seiten">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference master-reference="PageMaster.Inhalt-rechts" page-position="first" odd-or-even="odd"/>
            <fo:conditional-page-master-reference master-reference="PageMaster.Inhalt-links" page-position="rest" odd-or-even="even"/>
            <fo:conditional-page-master-reference master-reference="PageMaster.Inhalt-rechts" page-position="rest" odd-or-even="odd"/>
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="titlepage" initial-page-number="1">
        <fo:title>
            Kapitel <xsl:value-of select="count(preceding::d:chapter) + 1"/>
        </fo:title>
        <fo:flow flow-name="xsl-region-body">
          <fo:block font-family="AlegreyaSansSC" font-size="20pt" text-align="center" space-after="20pt">
            Kapitel <xsl:value-of select="count(preceding::d:chapter) + 1"/>
          </fo:block>
          <fo:block font-family="AlegreyaSans" font-size="14pt" text-align="right">
            <xsl:value-of select="/d:book/d:info/d:author/d:personname/d:firstname"/>
            <xsl:value-of select="/d:book/d:info/d:author/d:personname/d:surname"/>
          </fo:block>
          <fo:block font-family="AlegreyaSansSC" font-size="14pt" space-before="3cm" text-align="center">
		  Orkinos-Verlag
          </fo:block>
          <xsl:call-template name="generate-table-of-contents"/>
        </fo:flow>
      </fo:page-sequence>
      <xsl:apply-templates select="d:chapter|d:colophon|d:preface"/>
    </fo:root>
  </xsl:template>
  <xsl:template match="d:chapter">
    <fo:page-sequence master-reference="Inhalt-Seiten" force-page-count="even" id="{generate-id(.)}">
      <fo:title>
            Kapitel <xsl:value-of select="count(preceding::d:chapter) + 1"/>
      </fo:title>
      <fo:static-content flow-name="Inhalt-rechts-danach">
        <fo:block text-align="left" font-size="{$body.font.size}" font-family="Alegreya"><fo:page-number/> -
            Kapitel <xsl:value-of select="count(preceding::d:chapter) + 1"/>
          </fo:block>
      </fo:static-content>
      <fo:static-content flow-name="Inhalt-links-danach">
        <fo:block text-align="right" font-size="{$body.font.size}" font-family="Alegreya">
            Kapitel <xsl:value-of select="count(preceding::d:chapter) + 1"/>
            - <fo:page-number/>
          </fo:block>
      </fo:static-content>
      <fo:flow flow-name="xsl-region-body">
        <fo:block font-size="{$body.font.size}" text-align="center" font-weight="bold" font-family="AlegreyaSans" page-break-before="right" space-after="1em">
            Kapitel <xsl:value-of select="count(preceding::d:chapter) + 1"/>
        </fo:block>
        <xsl:apply-templates/>
      </fo:flow>
    </fo:page-sequence>
  </xsl:template>
  <xsl:template match="d:preface">
    <fo:page-sequence master-reference="Inhalt-Seiten" initial-page-number="1" force-page-count="even" id="{generate-id(.)}">
      <fo:title>
        <xsl:value-of select="/d:book/d:title"/>
      </fo:title>
      <fo:static-content flow-name="Inhalt-rechts-danach">
        <fo:block text-align="left" font-size="{$body.font.size}" font-family="Alegreya"><fo:page-number/> -
          <xsl:value-of select="d:title"/>
          </fo:block>
      </fo:static-content>
      <fo:static-content flow-name="Inhalt-links-danach">
        <fo:block text-align="right" font-size="{$body.font.size}" font-family="Alegreya"><xsl:value-of select="d:title"/>
            - <fo:page-number/>
          </fo:block>
      </fo:static-content>
      <fo:flow flow-name="xsl-region-body">
        <fo:block font-size="{$body.font.size}" text-align="center" font-weight="bold" font-family="AlegreyaSans" page-break-before="right" space-after="1em">
          <xsl:value-of select="d:title"/>
        </fo:block>
        <xsl:apply-templates/>
      </fo:flow>
    </fo:page-sequence>
  </xsl:template>
  <xsl:template match="d:colophon">
    <fo:page-sequence master-reference="Inhalt-Seiten" force-page-count="even" id="{generate-id(.)}">
      <fo:title>
        <xsl:value-of select="/d:book/d:title"/>
      </fo:title>
      <fo:static-content flow-name="Inhalt-rechts-danach">
        <fo:block text-align="left" font-size="{$body.font.size}" font-family="Alegreya"><fo:page-number/> - Kolophon
          </fo:block>
      </fo:static-content>
      <fo:static-content flow-name="Inhalt-links-danach">
        <fo:block text-align="right" font-size="{$body.font.size}" font-family="Alegreya">
            Kolophon - <fo:page-number/>
          </fo:block>
      </fo:static-content>
      <fo:flow flow-name="xsl-region-body">
        <fo:block font-size="{$body.font.size}" text-align="center" font-weight="bold" font-family="AlegreyaSans" page-break-before="right" space-after="1em">
	    Kolophon
        </fo:block>
        <xsl:apply-templates/>
      </fo:flow>
    </fo:page-sequence>
  </xsl:template>
  <xsl:template match="text()">
    <xsl:value-of select="."/>
  </xsl:template>
  <xsl:template match="d:title"/>
  <xsl:template match="d:para">
    <fo:block font-size="{$body.font.size}" line-height="{$body.line.height}" font-family="{$body.font.family}" space-before="{$body.start.vspace}" text-indent="{$body.start.indent}" text-align="justify">
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>
  <xsl:template match="d:emphasis">
    <fo:inline font-style="italic">
      <xsl:apply-templates/>
    </fo:inline>
  </xsl:template>
</xsl:stylesheet>
