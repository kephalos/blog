<?xml version="1.0"?>
<article xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink">
  <info>
    <title>Objects All The Way Down</title>
    <titleabbrev>objects-all-the-way-down.html</titleabbrev>
    <abstract>
      <para>
After researching <link xlink:href="http://www.holger-peters.de/why-i-use-pytest.html">the backgrounds of Python's unittest
module</link> I got curious
about <link xlink:href="http://en.wikipedia.org/wiki/Smalltalk">Smalltalk</link>, one of the
first truly object-oriented programming languages. I looked into it
(more specific, I played around with <productname>Pharo</productname>) and if you like,
this blog post will take you along this journey using a <link xlink:href="https://en.wikipedia.org/wiki/Rosetta_Stone">Rosetta
Stone</link> approach by writing
Python and Smalltalk snippets next to each other.
</para>
    </abstract>
    <author>
      <personname>
        <firstname>Holger</firstname>
        <surname>Peters</surname>
      </personname>
    </author>
    <authorinitials>hp</authorinitials>
    <pubdate>2015-11-22 23:12</pubdate>
  </info>
  <para>
After researching <link xlink:href="http://www.holger-peters.de/why-i-use-pytest.html">the backgrounds of Python's unittest
module</link> I got curious
about <link xlink:href="http://en.wikipedia.org/wiki/Smalltalk">Smalltalk</link>, one of the
first truly object-oriented programming languages. I looked into it
(more specific, I played around with <productname>Pharo</productname>) and if you like,
this blog post will take you along this journey using a <link xlink:href="https://en.wikipedia.org/wiki/Rosetta_Stone">Rosetta
Stone</link> approach by writing
Python and Smalltalk snippets next to each other
<footnote><para>
It was inspired by Simula, which is obviously prior art
</para></footnote>.
</para>
  <para>
Smalltalk was first released in 1972 and I was surprised when I realized
how advanced and radical its take on object-orientation is. It seems
more advanced than a lot of today's languages in a lot of aspects.
</para>
  <para>
I'll just start by introducing you to Smalltalk's syntax by comparison
with the most common syntax elements:
</para>
  <programlisting><![CDATA[
class MyClass(object):
def a_methodcall(x):
a = list(range(10, 1, -1))
a.sort()
return a
]]></programlisting>
  <para>
And now the implementation of the method in Smalltalk (for the class
definition see<footnote><para>
To help you read along these snippets: The caret <literal>^</literal> is
Smalltalk's <literal>return</literal>, the <literal>[...]</literal> sections are "blocks", comparable
to anonymous functions or indented parts in Python:
</para><para>
A class is declared in Smalltalk in the class browser. The
declaration is actually a method call on the superclass. I.e.
creating a subclass <classname>User</classname> involves calling the
<methodname>subclass:instanceVariableNames:classVariableNames:category</methodname> method
on the superclass <classname>Object</classname>.

</para><programlisting>
Object subclass: #User
  instanceVariableNames: 'name'
  classVariableNames: ''
  category: 'blogexample'
</programlisting><para>
Since messages (methods) are also written in the object browser,
they are not syntactically associated with the object declaration
(not in the way as python defines methods by having them indented in
the class definition).

</para><para>
A conventional way of showing class association of object names is
to write <methodname>ClassName&gt;&gt;messageName</methodname>. I did this in the snippets. If
you write them in the Pharo System Browser, you should ignore the
<literal>ClassName&gt;&gt;</literal> part when entering the message.
</para></footnote>)
</para>
  <programlisting><![CDATA[
MyClass>>aMethodCall
|a|
a := (10 to: 1 by: -1) asArray
^ a sort
]]></programlisting>
  <para>
First of all, the code is not that different. We create a list of values
from 10, 9, ... to 1 in Python which we convert to a list object. In
Smalltalk, we do the same, however, we do not invoke a function like
range, but we call the <methodname>to:by:</methodname> method of the integer class (think of
this as if we called <literal>10.to_by(to=1, by=-1)</literal> in Python). Calling a
method <methodname>asArray</methodname> then converts an intermediate representation of the
range into an array. Finally, we sort the list calling the <methodname>sort</methodname> method
of the Array object in Smalltalk, just as we sort the list in Python
calling the same method. The Smalltalk representation then returns the
result of the sort using its return operator <literal>^</literal>, as does the Python
version using <literal>return</literal>.
</para>
  <para>
So far we have seen that Python and Smalltalk are kind of similar,
although Smalltalk has a funny way of naming and calling methods,
dispersing method arguments over the method name <literal>10 to: 1 by: -1</literal>.
</para>
  <section>
    <title>
Every value is an object, every operation a method call
</title>
    <para>
The next snippet shows how Smalltalk really treats everything as an
object, radically. Take the following class <classname>User</classname> with a method that
returns a string representation for that user (maybe this is what a UI
would display then). If a username is undefined, it will return a string
that identifies the user as an anonymous user, otherwise it will
directly return the user name.
</para>
    <programlisting><![CDATA[
class User(object):
def repr_name(self):
"Return the name of the user if available, otherwise 'anonymous user'"
if self.name is None:
   res = "anonymous user"
else:
   res = self.name
return res

class User2(object):
def repr_name(self):
return "anonymous user" if self.name is None else self.name
]]></programlisting>
    <para>
They translate directly into two Smalltalk methods
</para>
    <programlisting><![CDATA[
User>>getRepresentativeName
"I return the name of the user if available, otherwise 'anonymous user'"
|res|
name == nil
ifTrue:  [res := 'anonymous user']
ifFalse: [res :=  name].
^ res
]]></programlisting>
    <para>
and in a more functional style with an evaluating expression:
</para>
    <programlisting><![CDATA[
User2>>getRepresentativeName
"I return the name of the user if available, otherwise 'anonymous user'"
^ name == nil
ifTrue: 'anonymous user'
ifFalse: name.
]]></programlisting>
    <para>
So the boolean class of Smalltalk has methods <methodname>ifTrue:ifFalse</methodname>,
<methodname>ifTrue</methodname>, etc. that can replace a special syntax for conditionals like
the one Python has. Using the same approach (implementing methods), we
can also write list-comprehension-style expressions -- with the
difference that in Smalltalk no special syntax is necessary. This
example here is a solution for the <link xlink:href="https://projecteuler.net/problem=1">first project euler
problem</link>:
</para>
    <programlisting><![CDATA[
divisibleRange := (1 to: N - 1) select: [ :i | i % 3 = 0 or: [ i % 5 = 0 ] ].
sumOfMultiples := divisibleRange inject: 0
                       into: [ :subTotal :item | subTotal + item ].
]]></programlisting>
    <programlisting><![CDATA[
divisible_range = (i for i in range(1, N) if i % 3 == 0 or i % 5 == 0)
from functools import reduce # needed for python 3 (WTF)
sum_of_multiples = reduce(lambda sub_total, item: sub_total + item,
                divisible_range,
                0)
]]></programlisting>
    <para>
I won't withhold, that I am pretty delighted at the fact, that Smalltalk
is so flexible, that it can express stuff like list-comprehension, loops
and conditionals in a similar, but probably more readable way like
Python, however with fewer and simpler syntax elements. I particularly
find Smalltalk's method-based syntax <methodname>inject:into:</methodname> to be one of the
most readable ways to formulate a <methodname>reduce</methodname> operation<footnote><para>
Although the Bdfl is known to dislike reduce altogether (markup is mine):
<blockquote><attribution>Guido van Rossum at <link xlink:href="http://www.artima.com/weblogs/viewpost.jsp?thread=98196">http://www.artima.com/weblogs/viewpost.jsp?thread=98196</link></attribution><para>
So now <methodname>reduce()</methodname>. This is actually the one I've always hated
most, because, apart from a few examples involving <methodname>+</methodname> or <methodname>*</methodname>,
almost every time I see a <methodname>reduce()</methodname> call with a non-trivial
function argument, I need to grab pen and paper to diagram what's
actually being fed into that function before I understand what the
<methodname>reduce()</methodname> is supposed to do. So in my mind, the applicability of
<methodname>reduce()</methodname> is pretty much limited to associative operators, and in
all other cases it's better to write out the accumulation loop
explicitly.
</para></blockquote>
</para><para>
I think they are actually better than a spelled-out accumulation
loop, because typically in a code-base that accumulation loop will
soon be stuffed with lot's of other code that has nothing to do with
the accumulation and it can become quite hard, even with pen and
paper, to be sure to understand what that loop does.
</para></footnote>.
</para>
  </section>
  <section>
    <title>
Object Oriented But On Which Level?
</title>
    <para>
So far we have seen mostly, that Smalltalk chooses sending "messages"
(Smalltalk lingo for "calling methods") for a lot of cases where
languages like Python implement special syntax. We have also seen that
this special syntax is not necessarily more powerful than Smalltalk's
message-based approach. On the other hand, apart from having less syntax
elements to learn, it might not be immediately obvious whether
Smalltalk's approach is more beneficial or less beneficial than the
custom-syntax one.
</para>
    <para>
Let us consider an example, Python has this interesting property (call
it a hack or feature according to taste) that you can define a
<methodname>__bool__</methodname> method for your class (Python 3, <methodname>__nonzero__</methodname> in Python 2).
In the if statement, it seems that Python calls <methodname>bool(obj)</methodname>, which in
the following case then dispatches to <methodname>obj.__bool__</methodname>:
</para>
    <programlisting><![CDATA[
class Test:
def __bool__(self):
return True

if Test():
print("hello")  # <- is executed
]]></programlisting>
    <para>
compare this to an established "overloading":
</para>
    <programlisting><![CDATA[
if "":
print("hello")  # <- is not executed
else:
print("world")  # <- is executed
]]></programlisting>
    <para>
The whole approach of Python is kind of weird here, nevertheless, the
result is that types can define for themselves how they are interpreted.
Using Smalltalk's object-oriented approach, it is quite clear that
overloading the <methodname>ifTrue:ifElse:</methodname> message suffices at achieving such a
behaviour. This is definitely simpler than Python's approach<footnote><para>
In the case of true-false, the principle here is called
truthiness, which by itsself might be debatable. I probably would
rather have the programmer explicitly state the intention of a
string being not empty instead of implicitly relying on a
"truthiness" setting.
</para></footnote>
</para>
    <para>
But what can we take from that? I have concluded, that the
object-oriented approach, that I had previously considered to be
concerned with architecture, i.e. the interplay of various code units.
In languges like Python, Java and C++, there is a tendency of writing
object-oriented code in the large and structured programming code within
the methods. In Smalltalk, I can see how object-orientation can also be
used within these units. At this micro-level, some properties of
object-oriented programming that I am not fond of, such as
state-encapsulation become less and less important, which resonates with
my preference of functional programming patterns.
</para>
  </section>
  <section>
    <title>
Getting Rid of Source Files
</title>
    <para>
Another interesting (and radical) take on software development in
Smalltalk is its approach to source files. In short: Smalltalk is
usually not edited in source files. Instead, the IDE is not only the
editor that you open files in, but instead it is more of a source code
database that stores objects and methods in an image file.
</para>
    <para>
Obviously, this approach never took hold, to this day, we are editing
code in source files. Nevertheless, it was the inspiration for the IDEs
we know today, like Eclipse, Pycharm, etc.
</para>
    <para>
In Smalltalk, editing takes place in a central IDE with a class and
method browser (see <xref linkend="ex.pythagorean"/>).
<figure xml:id="ex.pythagorean" pgwide="1"><title>Smalltalk IDE</title><mediaobject><imageobject condition="print"><imagedata fileref="../../assets/images/smalltalk-as-ide.png"/></imageobject></mediaobject></figure>
</para>
    <para>
Ian Bicking has two blog posts that identify this as the reason why Smalltalk did not catch on      (
<link xlink:href="http://www.ianbicking.org/where-smalltalk-went-wrong.html">
</link>
and
<link xlink:href="http://www.ianbicking.org/where-smalltalk-went-wrong-2.html">
</link>
). I am not sure if this was the reason back then in the 80ies, but I do think that this might be a reason in the Github-driven software world today.
</para>
  </section>
  <section>
    <title>
Conclusions
</title>
    <para>
The Smalltalk ecosystem looks dated. If you google for information,
you'll stumble over a lot of pages that seem to come right from the
90ies. Some of those go at great lengths of explaining concepts of
Smalltalk to C programmers, which is tiresome if you are already used to
dynamic, object-oriented typing.
</para>
    <para>
Working in an image conflicts with my typical usage of version control
while programming, which kind of rules out Smalltalk for me.
</para>
    <para>
What surprised me about Smalltalk is, how many 'functional' elements the
language has. It is actually less awkward to write a lambda function in
Smalltalk than in Scheme or Common Lisp, and the core APIs of the
built-in classes seem to rely on them heavily as well.
</para>
    <para>
I had seen object-orientation as an architectural feature of programming
languages, meaning that I considered object-orientation to be more about
structuring code on a larger scale, and not about structuring it on the
level within a function/method level. Looking at Smalltalk I gained a
better understanding on how object-oriented concepts can be used on all
scopes.
</para>
    <para>
In a way the interesting part about this comparison is the question
whether a minimal, expressive syntax is preferable over a specialized,
more complex syntax, that is fine-tuned for convenience. I'll just leave
the judgment up to you, just remember the <link xlink:href="https://www.python.org/doc/humor/#the-zen-of-python">Zen of
Python's</link> words:
<blockquote><attribution><link xlink:href="https://www.python.org/doc/humor/#the-zen-of-python">Zen of
Python's</link></attribution><para>
(...)
Readability counts. Special cases aren't special enough to break the
rules.
(...)
</para></blockquote>
</para>
    <para>
But if you are curious, go on and download Pharo and check it
out.
</para>
  </section>
</article>
